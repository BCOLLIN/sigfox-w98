package controllers;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.MonModel;

public class MonController implements ActionListener {
	
	private MonModel leModel;
	

	public MonController(MonModel leModel) {
		this.leModel = leModel;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {

		System.out.println("on est dans le controleur qui m'a reveill� : "+arg0.getActionCommand());
		
		switch (arg0.getActionCommand()) {
			case "BA" :
				leModel.traitementA();
				break;
			case "BB" :
				leModel.traitementB();
				break;
			default : 
				System.out.println("action inconnue");
		
		}

	}

}
