package views;
import javax.swing.JFrame;
import javax.swing.JLabel;

import model.MonModel;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class View1 extends JFrame implements Observer {
	
	private Container monContainerPrincipal = getContentPane();
	private JButton btnButtonA = new JButton("Bouton A");
	private JButton btnButtonB = new JButton("Bouton B");
	private JLabel etiquette = new JLabel("ici etiquette de donn�e");
	
	
	public View1() {
		
		setSize(400,200);
		
		// D�finition du container
		monContainerPrincipal.setLayout(new FlowLayout());
		
		//
		btnButtonA.setActionCommand("BA");
		monContainerPrincipal.add(btnButtonA);
		
		
		btnButtonB.setActionCommand("BB");
		monContainerPrincipal.add(btnButtonB);
		
		monContainerPrincipal.add(etiquette);
	}
	
	// je dis � mes boutons que c'est al qui va etre � l'�coute de leurs �v�nements
	public void setListener(ActionListener al) {
		btnButtonA.addActionListener(al);
		btnButtonB.addActionListener(al);
	}

	@Override
	public void update(Observable o, Object arg) {
		
		System.out.println("on me previent que le mod�le a chang�"+ ((MonModel) o).getMessageADiffuser());
		this.etiquette.setText(((MonModel) o).getMessageADiffuser());
		
	}
	
	

}
