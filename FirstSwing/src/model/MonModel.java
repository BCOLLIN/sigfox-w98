package model;
import java.util.Observable;

public class MonModel extends Observable {
	
	private String msgA = "message suite � action A";
	private String msgB = "message suite � action B";
	
	private String messageADiffuser = "";

	public MonModel() {
		
	}

	public String getMsgA() {
		return msgA;
	}

	public void setMsgA(String msgA) {
		this.msgA = msgA;
	}

	public String getMsgB() {
		return msgB;
	}

	public void setMsgB(String msgB) {
		this.msgB = msgB;
	}

	public String getMessageADiffuser() {
		return messageADiffuser;
	}

	public void setMessageADiffuser(String messageADiffuser) {
		this.messageADiffuser = messageADiffuser;
	}
	
	// METHODES AUTRES
	
	private void prevenirLesVues() {
		setChanged();
		notifyObservers();
	}
	
	public void traitementA() {
		System.out.println("traitement A");
		this.messageADiffuser = this.msgA;
		prevenirLesVues();
	}
	
	public void traitementB() {
		System.out.println("traitement B");
		this.messageADiffuser = this.msgB;
		prevenirLesVues();
	}
	
	


}
