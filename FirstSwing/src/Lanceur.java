import controllers.MonController;
import model.MonModel;
import views.View1;

public class Lanceur {

	public static void main(String[] args) {
		
		MonModel leM = new MonModel();
		MonController theC = new MonController(leM);
		
		View1 maVue = new View1();
		// je presente mon controleur � la vue
		maVue.setListener(theC);
		
		// j'ajoute ma vue  � la liste des observeurs de mon modele
		leM.addObserver(maVue);
		
		// je demande l'affichage de ma vue
		maVue.setVisible(true);
	}

}
